#include "carro.hpp"

Carro::Carro(){
    cout << "Criou o carro" << endl;
    velocidadeAtual = 0.0;
    velocidadeMax =  220.0;
}
Carro::~Carro()
{
    cout << "Destriu o carro" << endl;
}

void Carro::setCapacidade(int qtePessoas)
{
    if (qtePessoas > 0){
        capacidade = qtePessoas;
    }
}
int Carro::getCapacidade()
{
    return this->capacidade;
}
float Carro::getvelocidadeAtual()
{
    return this->velocidadeAtual;
}
float Carro::getvelocidadeMax()
{
    return this->velocidadeMax;
}
void Carro::setvelocidadeAtual(float velocidade)
{
    velocidadeAtual=velocidade;
}
void Carro::setvelocidadeMax(float velocidade)
{
    velocidadeMax=velocidade;
}